const express = require('express');
const bodyParser = require('body-parser');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');
const cors = require('cors')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const APP_SECRET = 'abcdefghijklmnopqrst';

// Some fake data
const products = [
  {
    id: '1',
    title: "Laptop",
    description: 'New laptop',
  },
  {
    id: '2',
    title: 'Book Jurassic Park',
    description: 'author: Michael Crichton',
  },
];

const users = [
  {
    id: '1',
    email: "john@gmail.com",
    password: '$2y$12$TAlrxoL5xexBNuzt6fQ4FO0zJlYcXhDlVGByfiRKzL4vdwjOkLG6a'
  },
  {
    id: '2',
    email: 'tom@gmail.com',
    password: '$2y$12$G.XzOrkkRPhBrVwvAoLYKucxy5en3eEIHEMHzvp2xdPFmvoE2ye5i',
  },
];

// The GraphQL schema in string form
const typeDefs = `
  type Query { products: [Product] }
  type Product { title: String, description: String, id: String }
  type User { id: String, email: String, password: String }
  type AuthPayload {
    user: User
    token: String
  }
  type Mutation {
    login(email: String, password: String): AuthPayload
  }
`;

// The resolvers
const resolvers = {
  Query: { products: () => products },
  Mutation: {
    login: async (root, data) => {
      const user = users.find(user => user.email === data.email);
      if (!user) {
        throw new Error('User not found');
      }
      const validPassword = await bcrypt.compare(data.password, user.password);
      if (!validPassword) {
        throw new Error('Password is incorrect');
      }
      const token = jwt.sign({ userId: user.id }, APP_SECRET);
      return {
        token,
        user
      };
    }
  },
};

// Put together a schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

// Initialize the app
const app = express();

app.use(cors());
// The GraphQL endpoint
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

// GraphiQL, a visual editor for queries
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

module.exports = app;
